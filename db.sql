CREATE TABLE "status" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    status_name VARCHAR NOT NULL UNIQUE
);

INSERT INTO "status" (status_name) VALUES ('OK'), ('MODERATED'), ('ARCHIVED');

CREATE OR REPLACE FUNCTION id_in_status(status varchar) RETURNS UUID LANGUAGE SQL AS
$$ SELECT id FROM status WHERE status = status; $$;

CREATE TABLE "user" (
    id UUID PRIMARY KEY,
    user_name citext NOT NULL UNIQUE,
    firstname citext NOT NULL,
    lastname citext NOT NULL,
    "description" VARCHAR NOT NULL,
    avatar_url VARCHAR NOT NULL,
    user_status UUID NOT NULL DEFAULT id_in_status('OK'),
    FOREIGN KEY (user_status) REFERENCES "status" (id)
);

CREATE TABLE "post" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    photo_url VARCHAR NOT NULL,
    content VARCHAR NOT NULL,
    post_status UUID NOT NULL DEFAULT id_in_status('OK'),
    FOREIGN KEY (user_id) REFERENCES "user" (id),
    FOREIGN KEY (post_status) REFERENCES "status" (id)
);

CREATE TABLE "follow" (
	id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    follower_id UUID,
    followee_id UUID,
    FOREIGN KEY (follower_id) REFERENCES "user" (id),
    FOREIGN KEY (followee_id) REFERENCES "user" (id)
);

CREATE TABLE "like" (
	id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID,
    post_id UUID,
    FOREIGN KEY (user_id) REFERENCES "user" (id),
    FOREIGN KEY (post_id) REFERENCES "post" (id)
);

CREATE TABLE "comment" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    post_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    content VARCHAR NOT NULL,
    comment_status UUID NOT NULL DEFAULT id_in_status('OK'),
    FOREIGN KEY (user_id) REFERENCES "user" (id),
    FOREIGN KEY (post_id) REFERENCES "post" (id),
    FOREIGN KEY (comment_status) REFERENCES "status" (id)
);